<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Imports\CdrImport;
use Http;
class Phptest extends Controller
{
    function parseFile(Request $request){
       
        
        Excel::import(new CdrImport, request()->file('file'));

        //notify to ws_server to refresh data
         Http::get('http://localhost:3000/update');
         
         return response()->json(['ok' => true],200);
                

    }
}
