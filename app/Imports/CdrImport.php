<?php

namespace App\Imports;

use App\Models\Cdr;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Http;
use  libphonenumber\NumberParseException;

class CdrImport implements ToModel, WithBatchInserts, WithChunkReading
{
    
    private  $numberUtil = null;
    private $counryIso2Continent = [
        'AD' => 'EU',
        'AE' => 'AS',
        'AF' => 'AS',
        'AG' => 'NA',
        'AI' => 'NA',
        'AL' => 'EU',
        'AM' => 'AS',
        'AO' => 'AF',
        'AQ' => 'AN',
        'AR' => 'SA',
        'AS' => 'OC',
        'AT' => 'EU',
        'AU' => 'OC',
        'AW' => 'NA',
        'AX' => 'EU',
        'AZ' => 'AS',
        'BA' => 'EU',
        'BB' => 'NA',
        'BD' => 'AS',
        'BE' => 'EU',
        'BF' => 'AF',
        'BG' => 'EU',
        'BH' => 'AS',
        'BI' => 'AF',
        'BJ' => 'AF',
        'BL' => 'NA',
        'BM' => 'NA',
        'BN' => 'AS',
        'BO' => 'SA',
        'BQ' => 'NA',
        'BR' => 'SA',
        'BS' => 'NA',
        'BT' => 'AS',
        'BV' => 'AN',
        'BW' => 'AF',
        'BY' => 'EU',
        'BZ' => 'NA',
        'CA' => 'NA',
        'CC' => 'AS',
        'CD' => 'AF',
        'CF' => 'AF',
        'CG' => 'AF',
        'CH' => 'EU',
        'CI' => 'AF',
        'CK' => 'OC',
        'CL' => 'SA',
        'CM' => 'AF',
        'CN' => 'AS',
        'CO' => 'SA',
        'CR' => 'NA',
        'CU' => 'NA',
        'CV' => 'AF',
        'CW' => 'NA',
        'CX' => 'OC',
        'CY' => 'EU',
        'CZ' => 'EU',
        'DE' => 'EU',
        'DJ' => 'AF',
        'DK' => 'EU',
        'DM' => 'NA',
        'DO' => 'NA',
        'DZ' => 'AF',
        'EC' => 'SA',
        'EE' => 'EU',
        'EG' => 'AF',
        'EH' => 'AF',
        'ER' => 'AF',
        'ES' => 'EU',
        'ET' => 'AF',
        'FI' => 'EU',
        'FJ' => 'OC',
        'FK' => 'SA',
        'FM' => 'OC',
        'FO' => 'EU',
        'FR' => 'EU',
        'GA' => 'AF',
        'GB' => 'EU',
        'GD' => 'NA',
        'GE' => 'AS',
        'GF' => 'SA',
        'GG' => 'EU',
        'GH' => 'AF',
        'GI' => 'EU',
        'GL' => 'NA',
        'GM' => 'AF',
        'GN' => 'AF',
        'GP' => 'NA',
        'GQ' => 'AF',
        'GR' => 'EU',
        'GS' => 'AN',
        'GT' => 'NA',
        'GU' => 'OC',
        'GW' => 'AF',
        'GY' => 'SA',
        'HK' => 'AS',
        'HM' => 'AN',
        'HN' => 'NA',
        'HR' => 'EU',
        'HT' => 'NA',
        'HU' => 'EU',
        'ID' => 'AS',
        'IE' => 'EU',
        'IL' => 'AS',
        'IM' => 'EU',
        'IN' => 'AS',
        'IO' => 'AS',
        'IQ' => 'AS',
        'IR' => 'AS',
        'IS' => 'EU',
        'IT' => 'EU',
        'JE' => 'EU',
        'JM' => 'NA',
        'JO' => 'AS',
        'JP' => 'AS',
        'KE' => 'AF',
        'KG' => 'AS',
        'KH' => 'AS',
        'KI' => 'OC',
        'KM' => 'AF',
        'KN' => 'NA',
        'KP' => 'AS',
        'KR' => 'AS',
        'XK' => 'EU',
        'KW' => 'AS',
        'KY' => 'NA',
        'KZ' => 'AS',
        'LA' => 'AS',
        'LB' => 'AS',
        'LC' => 'NA',
        'LI' => 'EU',
        'LK' => 'AS',
        'LR' => 'AF',
        'LS' => 'AF',
        'LT' => 'EU',
        'LU' => 'EU',
        'LV' => 'EU',
        'LY' => 'AF',
        'MA' => 'AF',
        'MC' => 'EU',
        'MD' => 'EU',
        'ME' => 'EU',
        'MF' => 'NA',
        'MG' => 'AF',
        'MH' => 'OC',
        'MK' => 'EU',
        'ML' => 'AF',
        'MM' => 'AS',
        'MN' => 'AS',
        'MO' => 'AS',
        'MP' => 'OC',
        'MQ' => 'NA',
        'MR' => 'AF',
        'MS' => 'NA',
        'MT' => 'EU',
        'MU' => 'AF',
        'MV' => 'AS',
        'MW' => 'AF',
        'MX' => 'NA',
        'MY' => 'AS',
        'MZ' => 'AF',
        'NA' => 'AF',
        'NC' => 'OC',
        'NE' => 'AF',
        'NF' => 'OC',
        'NG' => 'AF',
        'NI' => 'NA',
        'NL' => 'EU',
        'NO' => 'EU',
        'NP' => 'AS',
        'NR' => 'OC',
        'NU' => 'OC',
        'NZ' => 'OC',
        'OM' => 'AS',
        'PA' => 'NA',
        'PE' => 'SA',
        'PF' => 'OC',
        'PG' => 'OC',
        'PH' => 'AS',
        'PK' => 'AS',
        'PL' => 'EU',
        'PM' => 'NA',
        'PN' => 'OC',
        'PR' => 'NA',
        'PS' => 'AS',
        'PT' => 'EU',
        'PW' => 'OC',
        'PY' => 'SA',
        'QA' => 'AS',
        'RE' => 'AF',
        'RO' => 'EU',
        'RS' => 'EU',
        'RU' => 'EU',
        'RW' => 'AF',
        'SA' => 'AS',
        'SB' => 'OC',
        'SC' => 'AF',
        'SD' => 'AF',
        'SS' => 'AF',
        'SE' => 'EU',
        'SG' => 'AS',
        'SH' => 'AF',
        'SI' => 'EU',
        'SJ' => 'EU',
        'SK' => 'EU',
        'SL' => 'AF',
        'SM' => 'EU',
        'SN' => 'AF',
        'SO' => 'AF',
        'SR' => 'SA',
        'ST' => 'AF',
        'SV' => 'NA',
        'SX' => 'NA',
        'SY' => 'AS',
        'SZ' => 'AF',
        'TC' => 'NA',
        'TD' => 'AF',
        'TF' => 'AN',
        'TG' => 'AF',
        'TH' => 'AS',
        'TJ' => 'AS',
        'TK' => 'OC',
        'TL' => 'OC',
        'TM' => 'AS',
        'TN' => 'AF',
        'TO' => 'OC',
        'TR' => 'AS',
        'TT' => 'NA',
        'TV' => 'OC',
        'TW' => 'AS',
        'TZ' => 'AF',
        'UA' => 'EU',
        'UG' => 'AF',
        'UM' => 'OC',
        'US' => 'NA',
        'UY' => 'SA',
        'UZ' => 'AS',
        'VA' => 'EU',
        'VC' => 'NA',
        'VE' => 'SA',
        'VG' => 'NA',
        'VI' => 'NA',
        'VN' => 'AS',
        'VU' => 'OC',
        'WF' => 'OC',
        'WS' => 'OC',
        'YE' => 'AS',
        'YT' => 'AF',
        'ZA' => 'AF',
        'ZM' => 'AF',
        'ZW' => 'AF',
        'CS' => 'EU',
        'AN' => 'NA',
    ];

    public function __construct(){
        $this->numberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
    }
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Cdr([
            'customer_id'     => $row[0],
            'datetime'    => $row[1],
            'duration' => $row[2],
            'number' => $row[3],
            'ip' => $row[4],
            'continent_phone' => $this->phoneToContinent($row[3]),
            'continent_ip' => $this->ipToContinent($row[4]),
        ]);
    }

    public function batchSize(): int
    {
        return 50;
    }

    public function chunkSize(): int
    {
        return 50;
    }

    private function ipToContinent($ip)
    {
         $response = Http::timeout(1)->withOptions([
            'verify' => false,
        ])->get('http://api.ipstack.com/' . $ip, [
            'access_key' => '2994ab6f2c5642a0c9dc8c40578707d2',

        ])->json();

        return !empty($response['continent_code']) ? $response['continent_code'] : null;
    }


    private function phoneToContinent($number)
    {



        try {
           
            $phoneNumber = $this->numberUtil->parse('+' . $number, null);
            $region  = $this->numberUtil->getRegionCodeForNumber($phoneNumber);
            return !empty($this->counryIso2Continent[$region]) ? $this->counryIso2Continent[$region] : null;
        } catch (NumberParseException $e) {

            return null;
        }
    }
}
