<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cdr extends Model
{
    protected $fillable = ['continent_ip','continent_phone','customer_id', 'datetime', 'duration', 'number', 'ip', 'country_iso'];
    protected $table = 'cdr';
   // protected $primaryKey = null;
    public $incrementing = false;
}
