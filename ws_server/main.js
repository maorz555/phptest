const StatModel = require('./Models/stat-model');

class Main {

    static init() {
        this.wsConnArr = []
        StatModel.init();
    }

    static addWsConn(ws) {
       
        this.wsConnArr.push(ws);
        this.refresh(ws);
    }

    static refresh(ws) {
      
        StatModel.getStatisticsData().then(function (rows, fields) {
            
            ws.send(JSON.stringify({rows:rows[0]}));
        }).catch(e=>console.log(e));
    }

    static refreshAll() {

        StatModel.getStatisticsData().then(function (rows, fields) {
            let rowsObj = JSON.stringify({rows:rows[0]});
           
            Main.wsConnArr = Main.wsConnArr.filter((ws)=>{
               let ok = false;
                if(ws.readyState==1) {
                    ws.send(rowsObj);   
                    ok= true;
                }

                return ok;

            })

        });

    }
}


module.exports = Main;