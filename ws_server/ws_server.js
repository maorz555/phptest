let express = require('express');
let app = express();
let expressWs = require('express-ws')(app);

let Main = require('./main');
Main.init();

app.get('/update', function (req, res, next) {
  
    Main.refreshAll();
    res.end();
});

app.ws('/', function(ws,req) {
    Main.addWsConn(ws);
});

app.listen(3000,() => console.log(`listening at http://localhost:3000`));
