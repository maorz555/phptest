const mysql = require('mysql2');

class StatModel {

    static init() {

        if (!this.conn) {
            
            try {
                this.conn = mysql.createConnection({
                    host: 'localhost',
                    user: 'root',
                    database: 'phptest'
                });
            } catch (error) {
                console.error(error);

            }
        }
    }

    static getStatisticsData() {

        return this.conn.promise().query('SELECT `customer_id` , \
            SUM(IF(`continent_ip`=`continent_phone`,1,0)) AS num_call_same_cont,\
            SEC_TO_TIME(SUM(IF(`continent_ip`=`continent_phone`,`duration`,0))) AS total_dur_same_cont,\
            COUNT(1) AS total_calls , \
            SEC_TO_TIME(SUM(duration)) AS total_dur \
             FROM `cdr` GROUP BY `customer_id`');




    }


}

module.exports = StatModel;