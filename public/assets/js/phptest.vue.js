Vue.use(VueMaterial.default);
Vue.use(VueLoading);
Vue.component('loading', VueLoading)

let app = new Vue({
  components:['table','loading'],
  el: '#app'
});
