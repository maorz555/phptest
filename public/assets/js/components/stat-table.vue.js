Vue.component('stat-table', {
    data: function () {
        return {
            rows: []
        }
    }, created: function () {
        console.log("Starting connection to WebSocket Server")
        this.connection = new WebSocket("ws://localhost:3000")

        this.connection.onmessage =  (event) =>{
            console.log(event.data);
            this.rows = JSON.parse(event.data).rows;
        }

      /*   this.connection.onopen = function (event) {
           
        } */

    },
    template: `<div>
    <md-table>
        <md-table-row>
            <md-table-head md-numeric>Customer ID</md-table-head>
            <md-table-head>Number of calls within same continent</md-table-head>
            <md-table-head>Total Duration of calls within same continent</md-table-head>
            <md-table-head>Total number of all calls</md-table-head>
            <md-table-head>Total duration of all calls</md-table-head>
        </md-table-row>

        <md-table-row  v-for="row in rows">
            <md-table-cell>{{row.customer_id}}</md-table-cell>
            <md-table-cell>{{row.num_call_same_cont}}</md-table-cell>
            <md-table-cell>{{row.total_dur_same_cont}}</md-table-cell>
            <md-table-cell>{{row.total_calls}}</md-table-cell>
            <md-table-cell>{{row.total_dur}}</md-table-cell>
        </md-table-row>

    
    </md-table>
</div>`
});