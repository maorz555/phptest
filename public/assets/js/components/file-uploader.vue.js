Vue.component('file-uploader', {
    data: function () {
        return {
            file: null,
        }
    },
    methods: {
        handleFileUpload() {
            this.file = this.$refs.file.files[0];
        },
        submitNandler: function () {
          
            let loader = this.$loading.show({
                // Optional parameters
                container: this.fullPage ? null : this.$refs.formContainer,
                canCancel: true,
                onCancel: this.onCancel,
              });

            let formData = new FormData();
            formData.append('file',  this.$refs.file.$refs.inputFile.files[0]);
            axios.post('/parseFile',
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            ).then(function () {
                loader.hide();
                console.log('SUCCESS!!');
            })
                .catch(function () {
                    console.log('FAILURE!!');
                });
        }
    },
    template: `
    <md-field>
      <label>Upload CSV file</label>
      <md-file id="file" ref="file" v-model="file" accept="csv"  />
      <md-button class="md-primary" @click="submitNandler">Submit</md-button>

    </md-field>
    `

});