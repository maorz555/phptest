<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/vue-material/dist/vue-material.min.css">
    <link rel="stylesheet" href="https://unpkg.com/vue-material/dist/theme/default.css">
    <link href="https://cdn.jsdelivr.net/npm/vue-loading-overlay@3/dist/vue-loading.css" rel="stylesheet">
    
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>

    <script src="https://cdn.jsdelivr.net/npm/vue-loading-overlay@3"></script>

    <script src="https://unpkg.com/vue-material"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script type="module" src="/assets/js/components/stat-table.vue.js" defer></script>
    <script type="module" src="/assets/js/components/file-uploader.vue.js" defer></script>
    <script type="application/javascript" src="/assets/js/phptest.vue.js" defer></script>
    <title>Php test</title>
</head>

<body>
    <div class="flex-center position-ref full-height">

        <div id="app" class="content">
            <file-uploader></file-uploader>
            <stat-table></stat-table>
        </div>
    </div>
</body>

</html>